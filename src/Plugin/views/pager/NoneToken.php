<?php

namespace Drupal\views_pager_token\Plugin\views\pager;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Attribute\ViewsPager;
use Drupal\views\Plugin\views\pager\None;

/**
 * Plugin for views without pagers. Available to enter token.
 *
 * @ingroup views_pager_plugins
 */
#[ViewsPager(
  id: "none_token",
  title: new TranslatableMarkup("Display all items (Available Token)"),
  help: new TranslatableMarkup("Display all items that this view might find."),
  display_types: ["basic"],
)]
final class NoneToken extends None {

  /**
   *
   */
  public function summaryTitle() {
    if (!empty($this->getOffset())) {
      return $this->t('All items, skip @skip', ['@skip' => $this->getOffset()]);
    }
    return $this->t('All items');
  }

  /**
   * {@inheritDoc}
   */
  protected function defineOptions() {
    $options = [];
    $options['view_pager_token__offset'] = ['default' => '0'];
    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    unset($form['offset']);
    $form['view_pager_token__offset'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Offset (number of items to skip)'),
      '#description' => $this->t('For example, set this to 3 and the first 3 items will not be displayed.'),
      '#default_value' => $this->options['view_pager_token__offset'],
    ];
    $form['help_token'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => 'all',
      '#global_types' => TRUE,
      '#click_insert' => TRUE,
      '#show_restricted' => FALSE,
      '#recursion_limit' => 3,
      '#text' => $this->t('Browse available tokens'),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getOffset() {
    return $this->replace($this->options['view_pager_token__offset'] ?? 0);
  }

  /**
   * Replace Token.
   */
  private function replace($argument): int {
    $token_service = \Drupal::token();
    $value = $token_service->replace($argument);
    if (is_numeric($value)) {
      return (int) $value;
    }
    return 0;
  }

  /**
   * {@inheritDoc}
   */
  public function query() {
    // The only query modifications we might do are offsets.
    if (!empty($this->getOffset())) {
      $this->view->query->setOffset($this->getOffset());
    }
  }

}
