<?php

namespace Drupal\views_pager_token\Plugin\views\pager;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Attribute\ViewsPager;
use Drupal\views\Plugin\views\pager\Some;

/**
 * Plugin for views without pagers. Available to enter token.
 *
 * @ingroup views_pager_plugins
 */
#[ViewsPager(
  id: "some_token",
  title: new TranslatableMarkup("Display a specified number of items (Available Token)"),
  help: new TranslatableMarkup("Display a limited number items that this view might find."),
  display_types: ["basic"],
)]
final class SomeToken extends Some {

  /**
   * {@inheritDoc}
   */
  protected function defineOptions() {
    $options = [];
    $options['views_pager_token__items_per_page'] = ['default' => '10'];
    $options['view_pager_token__offset'] = ['default' => '0'];
    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    unset($form['items_per_page'], $form['offset']);
    $pager_text = $this->displayHandler->getPagerText();
    $form['views_pager_token__items_per_page'] = [
      '#title' => $pager_text['items per page title'],
      '#type' => 'textfield',
      '#description' => $pager_text['items per page description'],
      '#default_value' => $this->options['views_pager_token__items_per_page'],
    ];

    $form['view_pager_token__offset'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Offset (number of items to skip)'),
      '#description' => $this->t('For example, set this to 3 and the first 3 items will not be displayed.'),
      '#default_value' => $this->options['view_pager_token__offset'],
    ];

    $form['help_token'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => 'all',
      '#global_types' => TRUE,
      '#click_insert' => TRUE,
      '#show_restricted' => FALSE,
      '#recursion_limit' => 3,
      '#text' => $this->t('Browse available tokens'),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getItemsPerPage() {
    return $this->replace($this->options['views_pager_token__items_per_page'] ?? '0');
  }

  /**
   * {@inheritDoc}
   */
  public function getOffset() {
    return $this->replace($this->options['view_pager_token__offset'] ?? '0');
  }

  /**
   * Replace Token.
   */
  private function replace($argument): int {
    $token_service = \Drupal::token();
    $value = $token_service->replace($argument);
    if (is_numeric($value)) {
      return (int) $value;
    }
    return 0;
  }

  /**
   * {@inheritDoc}
   */
  public function query() {
    $this->view->query->setLimit($this->getItemsPerPage());
    $this->view->query->setOffset($this->getOffset());
  }

}
